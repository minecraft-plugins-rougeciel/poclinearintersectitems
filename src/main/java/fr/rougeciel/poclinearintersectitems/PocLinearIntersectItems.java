package fr.rougeciel.poclinearintersectitems;

import fr.rougeciel.poclinearintersectitems.commands.ActivateCmd;
import fr.rougeciel.poclinearintersectitems.items.ShellsManager;
import fr.rougeciel.poclinearintersectitems.listeners.RightClickListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class PocLinearIntersectItems extends JavaPlugin {

    private static PocLinearIntersectItems instance;

    private ShellsManager shellsManager;
    private boolean activated;

    @Override
    public void onEnable() {
        instance = this;
        shellsManager = new ShellsManager(0.5F);
        activated = false;
        registerCommands();
        registerListeners();
    }

    private void registerCommands() {
        Bukkit.getPluginCommand("activate").setExecutor(new ActivateCmd());
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new RightClickListener(), this);

    }

    @Override
    public void onDisable() {
        shellsManager.destroyAll();
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public ShellsManager getShellsManager() {
        return shellsManager;
    }

    public static PocLinearIntersectItems getInstance() {
        return instance;
    }
}
