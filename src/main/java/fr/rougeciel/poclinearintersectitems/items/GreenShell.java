package fr.rougeciel.poclinearintersectitems.items;

import fr.rougeciel.poclinearintersectitems.PocLinearIntersectItems;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class GreenShell {

    private Player sender;
    private Location launchLocation;
    private Vector launchDirection;
    private final int maxTicks;
    private ArmorStand armorStand;

    public GreenShell(Player sender, Location launchLocation, Vector launchDirection) {
        this.sender = sender;
        this.launchLocation = launchLocation;
        this.launchDirection = launchDirection;
        this.maxTicks = 5;
        this.armorStand = null;

        shoot();
    }

    public void shoot() {
        summonArmorStand();
    }

    private void summonArmorStand() {
        Location senderLoc = sender.getLocation();
        armorStand = (ArmorStand) Bukkit.getWorld(senderLoc.getWorld().getUID()).spawnEntity(senderLoc, EntityType.ARMOR_STAND);
        armorStand.setGlowing(true);
        armorStand.setGravity(true);
        armorStand.getEquipment().setHelmet(new ItemStack(Material.TURTLE_HELMET));
    }

    public void destroy() {
        armorStand.remove();
    }

    public Player getSender() {
        return sender;
    }

    public Location getLaunchLocation() {
        return launchLocation;
    }

    public Vector getLaunchDirection() {
        return launchDirection;
    }

    public int getMaxTicks() {
        return maxTicks;
    }

    public ArmorStand getArmorStand() {
        return armorStand;
    }
}
