package fr.rougeciel.poclinearintersectitems.items;

import fr.rougeciel.poclinearintersectitems.PocLinearIntersectItems;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;

public class ShellsManager implements Runnable {

    private Integer taskId;
    private float tolerance;
    private HashSet<GreenShell> greenShells;

    public ShellsManager(float tolerance) {
        this.taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(PocLinearIntersectItems.getInstance(), this, 1, 0).getTaskId();
        this.tolerance = tolerance;
        this.greenShells = new HashSet<>();
    }

    @Override
    public void run() {
        moveShells();
    }

    public void launchShell(Player player, Location startLocation, Vector direction) {
        new GreenShell(player, startLocation, direction);
    }

    private void moveShells() {

    }

    public void destroyAll() {
        greenShells.forEach(GreenShell::destroy);
    }

    public HashSet<GreenShell> getGreenShells() {
        return greenShells;
    }

    public void setGreenShells(HashSet<GreenShell> greenShells) {
        this.greenShells = greenShells;
    }
}
