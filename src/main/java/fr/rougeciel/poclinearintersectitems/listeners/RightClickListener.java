package fr.rougeciel.poclinearintersectitems.listeners;

import fr.rougeciel.poclinearintersectitems.PocLinearIntersectItems;
import fr.rougeciel.poclinearintersectitems.items.ShellsManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class RightClickListener implements Listener {

    private static final ShellsManager shellsManager = PocLinearIntersectItems.getInstance().getShellsManager();

    @EventHandler
    public void onRightClickAir(PlayerInteractEvent event) {
        if (!PocLinearIntersectItems.getInstance().isActivated()) {
            return;
        }

        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR)) {
            return;
        }

        Player player = event.getPlayer();

        event.setCancelled(true);

        shellsManager.launchShell(player, player.getLocation(), player.getLocation().getDirection());
    }
}
