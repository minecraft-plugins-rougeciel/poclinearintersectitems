package fr.rougeciel.poclinearintersectitems.commands;

import fr.rougeciel.poclinearintersectitems.PocLinearIntersectItems;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ActivateCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        PocLinearIntersectItems pocLinearIntersectItems = PocLinearIntersectItems.getInstance();
        pocLinearIntersectItems.setActivated(!pocLinearIntersectItems.isActivated());
        sender.sendMessage("Status : " + pocLinearIntersectItems.isActivated());
        return false;
    }
}
